<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'toombatest');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '?bfo$Xmp$nmj2G5Zn50edkiPvdo=;,:y/&DQ=,)xYyP#<gL}c*d5jrC bwbU6n%S');
define('SECURE_AUTH_KEY',  '(d5H3N qh4t,v>/3>L_`D UB7Kc<kU2pWU-.]+{!<d:!FM2x&9)?^E!tK;7` 2j{');
define('LOGGED_IN_KEY',    'Iyg0.6.l}]jy+bxLo|D#g50-[ H:^^d#RKMvG/rgnoV06B{ A7cdix)32B#N!=AR');
define('NONCE_KEY',        '0Tg2m go;<x5iAnKcQ4B Z;8-n[wq}voMF$Kv$X!)%nI6H<=4uB{]<a{U?8S[GK(');
define('AUTH_SALT',        'Gh6=_8Be.S_A>$3by<}OB%EcPsSNl3T}TvU7r0{)Vk3lQP8|:QY;&*rf%z=,k^YB');
define('SECURE_AUTH_SALT', 'e4d% ypu9t/PEC*dJzce1`/5OJJF0=yYfep9,zGo!20f|H(ym?u7<)Q#-%<Q;~6K');
define('LOGGED_IN_SALT',   'QPE({pK~_N!&NCYkd$0l(,o&r,TSx}:^s1HD)A|.c1jKmZ)nOOzp~U<98uZD`WSQ');
define('NONCE_SALT',       '=6K9c-xOMV@|0?8lq-cZ1}UO{uT~YVRk.du+3Csdo>`/u g7ADU^7I!iJX}t[^8x');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
